<?php

/**
 * @file
 * Colorbox Node Caption theme functions.
 */
function theme_colorbox_node_caption($variables){
  $markup     = "<div class=\"cnc-caption cnc-caption-${variables['location']}\">${variables['caption']}</div>";
  return ( $variables['location'] == 'before' ) ? $markup . $variables['item'] : $variables['item'] . $markup;
}

/**
 * colorbox_node_caption_theme_colorbox_image_formatter
 * Theme override for theme_colorbox_image_formatter
 *
 * This is used to add the caption to the node display
 *
 * @param type $variables
 * @return type
 */
function colorbox_node_caption_theme_colorbox_image_formatter($variables) {
  $settings = $variables['display_settings']['fs_node_caption'];
  $cb_image = theme_colorbox_image_formatter( $variables );

  if( $settings['node_caption'] == 'none' ){
    return $cb_image;
  }else{
    $item   = $variables['item'];
    $node   = $variables['node'];

    $image    = array(
      'alt'     => $item['alt'],
      'title'     => $item['title'],
    );

    switch ($settings['node_caption']) {
      case 'auto':
        // If the title is empty use alt or the node title in that order.
        if (!empty($image['title'])) {
          $caption = $image['title'];
        }
        elseif (!empty($image['alt'])) {
          $caption = $image['alt'];
        }
        elseif (!empty($node->title)) {
          $caption = $node->title;
        }
        else {
          $caption = '';
        }
        break;
      case 'title':
        $caption = $image['title'];
        break;
      case 'alt':
        $caption = $image['alt'];
        break;
      case 'node_title':
        $caption = $node->title;
        break;
      case 'custom':
        $caption = token_replace($settings['node_caption_custom'], array('node' => $node));
        break;
      default:
        $caption = '';
    }
    return theme( 'colorbox_node_caption', array( 'item'=>$cb_image, 'caption'=>$caption, 'location'=>$settings['node_caption_location'] ) );
  }
}
